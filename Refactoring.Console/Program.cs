﻿using System;
using System.Collections.Generic;

namespace Refactoring.App
{
    class Program
    {
        private static IEnumerable<Filme> _filmes;
        private static Cliente cliente;

        static void Main(string[] args)
        {
            var starWars = new Filme("Star Wars: The Last Jedi", Genero.Lancamento);
            var toyStory = new Filme("Toy Story 3", Genero.Infantil);
            var oExtraordinario = new Filme("O Extraordinário", Genero.Normal);

            _filmes = new List<Filme>
            {
                starWars, toyStory, oExtraordinario
            };

            cliente = new Cliente("Nome Cliente");
            cliente.AdicionarEmprestimo(new Emprestimo(starWars, 5));
            cliente.AdicionarEmprestimo(new Emprestimo(toyStory, 3));
            cliente.AdicionarEmprestimo(new Emprestimo(oExtraordinario, 3));

            Console.WriteLine(cliente.GerarRecibo());

            Console.ReadLine();
        }
    }
}
