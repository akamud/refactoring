﻿using System;

namespace Refactoring
{
    public class Emprestimo
    {
        public Filme Filme  { get; private set; }
        public int DiasEmprestimo { get; private set; }

        public Emprestimo(Filme filme, int diasEmprestimo)
        {
            Filme = filme;
            DiasEmprestimo = diasEmprestimo;
        }
    }
}
