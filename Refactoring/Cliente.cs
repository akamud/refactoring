﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Refactoring
{
    public class Cliente
    {
        private static readonly CultureInfo ptBrCulture = new CultureInfo("pt-BR");

        public string Nome { get; private set; }

        public IList<Emprestimo> Emprestimos { get; set; }

        public decimal ValorTotal { get; private set; }

        public int PontuacaoFidelidade { get; private set; }

        public Cliente(string nome)
        {
            Nome = nome;
            Emprestimos = new List<Emprestimo>();
        }

        public void AdicionarEmprestimo(Emprestimo emprestimo)
        {
            Emprestimos.Add(emprestimo);
        }

        public string GerarRecibo()
        {
            string result = $"Registro de empréstimo para {Nome}\n";

            foreach (var emprestimo in Emprestimos)
            {
                decimal valor = 0;

                switch(emprestimo.Filme.Genero)
                {
                    case Genero.Normal:
                        valor += 2;

                        if (emprestimo.DiasEmprestimo > 2)
                            valor += (emprestimo.DiasEmprestimo - 2) * 1.5m;
                        break;
                    case Genero.Lancamento:
                        valor += emprestimo.DiasEmprestimo * 3;
                        break;
                    case Genero.Infantil:
                        valor += 1.5m;

                        if (emprestimo.DiasEmprestimo > 3)
                            valor += (emprestimo.DiasEmprestimo - 3) * 1.5m;
                        break;
                }

                PontuacaoFidelidade++;

                if (emprestimo.Filme.Genero == Genero.Lancamento && emprestimo.DiasEmprestimo > 1)
                    PontuacaoFidelidade++;

                result += $"\t{emprestimo.Filme.Titulo}\t{valor.ToString("C", ptBrCulture)}\n";

                ValorTotal += valor;
            }

            result += $"Valor devido: {ValorTotal.ToString("C", ptBrCulture)}\n";
            result += $"Você ganhou {PontuacaoFidelidade} pontos de fidelidade";

            return result;
        }
    }
}
