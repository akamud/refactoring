﻿using System;
namespace Refactoring
{
    public class Filme
    {
        public string Titulo { get; private set; }

        public Genero Genero { get; set; }

        public Filme(string titulo, Genero genero)
        {
            Titulo = titulo;
            Genero = genero;
        }
    }
}
